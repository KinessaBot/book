﻿using System;

namespace Book
{
    

    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("2000", "Big culinar book", "John Wamn");
            Book.SaveToJson("1.json", book);
            Book.SaveToXml("1.xml", book);

            Book book1 = Book.ReadFromJson("1.json");
            Console.WriteLine(book1.ToString());
            Book book2 = Book.ReadFromXml("1.xml");
            Console.WriteLine(book2.ToString());
            Console.ReadLine();
        }
    }
}
