﻿using System;
using System.Text;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Book
{
    [Serializable]
    public class Book
    {
        public string year;
        public string name;
        public string author;

        public override string ToString()
        {
            return year + " " + name + " " + author;
        }

        public Book(string year, string name, string author)
        {
            this.year = year;
            this.name = name;
            this.author = author;
        }

        public Book()
        {
            year = "";
            name = "";
            author = "";
        }

        public static Book ReadFromXml(string path)
        {
            Book book = new Book();
            using (XmlReader reader = XmlReader.Create(path, new XmlReaderSettings() { IgnoreWhitespace = true }))
            {
                reader.MoveToContent();
                reader.ReadStartElement("book");
                reader.ReadStartElement("year");
                book.year=(string)reader.ReadContentAsObject();
                reader.ReadEndElement();
                reader.ReadStartElement("name");
                book.name = (string)reader.ReadContentAsObject();
                reader.ReadEndElement();
                reader.ReadStartElement("author");
                book.author = (string)reader.ReadContentAsObject();
                reader.ReadEndElement();
                reader.ReadEndElement();

            }
            return book;
        }

        public static void SaveToXml(string path, Book book)
        {
            using (XmlWriter writer = XmlWriter.Create(path, new XmlWriterSettings() { Indent = true }))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("book");
                writer.WriteElementString("year", book.year);
                writer.WriteElementString("name", book.name);
                writer.WriteElementString("author", book.author);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        public static void SaveToJson(string path, Book book)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Newtonsoft.Json.Formatting.Indented;
                writer.WriteStartObject();
                writer.WritePropertyName("year");
                writer.WriteValue(book.year);
                writer.WritePropertyName("name");
                writer.WriteValue(book.name);
                writer.WritePropertyName("author");
                writer.WriteValue(book.author);
                writer.WriteEndObject();
            }
            File.WriteAllText(path, sb.ToString());

        }

        public static Book ReadFromJson(string path)
        {
            Book book = new Book();
            using (var json = new StreamReader(path))
            {
                var reader = new JsonTextReader(json);
                var jObj = JObject.Load(reader);
                book.year = jObj.GetValue("year").Value<string>();
                book.name = jObj.GetValue("name").Value<string>();
                book.author = jObj.GetValue("author").Value<string>();
            }
            return book;
        }
    }
}
